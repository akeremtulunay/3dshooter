using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public bool isShootingExplosive = false;
    public bool isShootingBig = false;
    public bool isShootingRed = false;
    public float spreadAngle = 15f;

    private void Awake()
    {       
        DontDestroyOnLoad(this.gameObject);
    }

    public void Start()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        SetConfigs();
    }

    public void ToggleExplosive()
    {
        isShootingExplosive = !isShootingExplosive;
        FindObjectOfType<ExplosiveToggleText>().ChangeText();
    }
    public void ToggleSize()
    {
        isShootingBig = !isShootingBig;
        FindObjectOfType<BulletSizeText>().ChangeText();
    }
    public void ToggleColor()
    {
        isShootingRed = !isShootingRed;
        FindObjectOfType<BulletColorText>().ChangeText();
    }
    public void ToggleAngle()
    {
        spreadAngle += 15f;
        if(spreadAngle > 60f)
        {
            spreadAngle = 15f;
        }
        FindObjectOfType<SpreadAngleText>().SetText(spreadAngle);
    }
    
    public void StartGame()
    {
        SceneManager.LoadScene("SampleScene");
    }

    void SetConfigs()
    {
        Rifle rifle = FindObjectOfType<Rifle>();
        if (isShootingExplosive)
        {
            print("explosive");
            rifle.isShootingExplosive = isShootingExplosive;
        }
        if (isShootingBig)
        {
            print("big");
            rifle.isShootingBig = isShootingBig;
        }
        if (isShootingRed)
        {
            print("red");
            rifle.isShootingRed = isShootingRed;
        }
        rifle.maxSpreadAngle = spreadAngle;
    }
}
