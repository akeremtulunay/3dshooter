using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rifle : MonoBehaviour
{
    public PlayerController player;
    public float range = 100f;
    public int minBulletCount = 1;
    public int maxBulletCount = 5;
    public int bulletsPerShot = 1;
    public float maxSpreadAngle = 30f;
    public Transform attackPoint;
    public GameObject bullet;
    public float bulletSpeed = 300;
    public BulletCounter counter;
    public bool isShootingBig = false;
    public bool isShootingExplosive = false;
    public bool isShootingRed = false;

    void Start()
    {
        counter = FindObjectOfType<BulletCounter>();
        player = GetComponentInParent<PlayerController>();
    }

    void Update()
    {
        CheckInput();
    }

    private void CheckInput()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Shoot();
            setCounterText();
        }
        if (Input.GetKeyDown(KeyCode.Z))
        {
            isShootingExplosive = !isShootingExplosive;
            FindObjectOfType<ExplosiveToggleText>().ChangeText();
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            isShootingBig = !isShootingBig;
            FindObjectOfType<BulletSizeText>().ChangeText();
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            isShootingRed = !isShootingRed;
            FindObjectOfType<BulletColorText>().ChangeText();
        }
    }

    private void Shoot()
    {
        bulletsPerShot = UnityEngine.Random.Range(minBulletCount, maxBulletCount + 1);
        for (int i = 0; i < bulletsPerShot; i++)
        {
            Vector3 directionWithSpread = CalculateDirection();
            ShootBullet(directionWithSpread);
        }
    }

    private Vector3 CalculateDirection()
    {
        Vector3 directionWithSpread;
        Vector3 directionWithoutSpread = transform.forward;
        float x = UnityEngine.Random.Range(-maxSpreadAngle, maxSpreadAngle);
        directionWithSpread = directionWithoutSpread + new Vector3(Mathf.Sin(x * Mathf.Deg2Rad), 0, 0);
        return directionWithSpread;
    }

    private void ShootBullet(Vector3 directionWithSpread)
    {
        GameObject currentBullet = Instantiate(bullet, attackPoint.position, Quaternion.identity);
        Bullet bulletComponent = currentBullet.GetComponent<Bullet>();
        ConfigBullet(bulletComponent);
        currentBullet.GetComponent<Rigidbody>().AddForce(directionWithSpread.normalized * bulletSpeed, ForceMode.Impulse);
    }

    private void ConfigBullet(Bullet bulletComponent)
    {
        bulletComponent.GetBigger(isShootingBig);
        bulletComponent.isExplosive = isShootingExplosive;
        bulletComponent.BecomeRed(isShootingRed);
    }

    private void setCounterText()
    {
        counter.SetText(bulletsPerShot.ToString());
    }
}
