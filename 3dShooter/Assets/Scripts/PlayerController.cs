using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    Vector3 velocity;
    public float speed = 3f;
    public CharacterController characterController;
    Camera mainCamera;

    void Start()
    {
        mainCamera = FindObjectOfType<Camera>();
    }

    void Update()
    {
        CheckInput();
        CheckMousePosition();
    }

    private void CheckInput()
    {
        ProcessMovement();      
    }

    private void CheckMousePosition()
    {
        RotatePlayer();
    }

    void ProcessMovement()
    {
        velocity = Input.GetAxisRaw("Horizontal") * Vector3.right + Input.GetAxisRaw("Vertical") * Vector3.forward;
        transform.position += velocity * speed * Time.deltaTime;
    }

    void RotatePlayer()
    {
        Ray cameraRay = mainCamera.ScreenPointToRay(Input.mousePosition);
        Plane groundPlane = new Plane(Vector3.up, Vector3.zero);
        float rayLength;

        if (groundPlane.Raycast(cameraRay, out rayLength))
        {
            Vector3 pointToLookAt = cameraRay.GetPoint(rayLength);
            transform.LookAt(new Vector3(pointToLookAt.x, transform.position.y, pointToLookAt.z));
        }
    }
}
