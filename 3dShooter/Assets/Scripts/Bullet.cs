using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public GameObject explosion;
    public float lifeTime = 1f;
    GameObject bulletExplosion;
    public bool isExplosive = false;
    public Material[] materials;

    void Start()
    {
        
    }

    void Update()
    {
        lifeTime -= Time.deltaTime;
        if(lifeTime <= 0)
        {
            if (isExplosive)
            {
                Explode();
            }

            Invoke("DestroySelf", 0.5f);
        }
    }

    private void Explode()
    {
        if(explosion != null)
        {
            bulletExplosion = Instantiate(explosion, transform.position, Quaternion.identity);
        }
        gameObject.SetActive(false);

    }

    private void DestroySelf()
    {
        if(bulletExplosion != null)
        {
            Destroy(bulletExplosion);
        }
        Destroy(gameObject);
    }

    public void GetBigger(bool isShootingBig)
    {
        if (isShootingBig)
        {
            transform.localScale *= 2;
        }
    }

    public void BecomeRed(bool isShootingRed)
    {
        MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
        if (isShootingRed)
        {
            meshRenderer.material = materials[1];
        }
        else
        {
            meshRenderer.material = materials[0];
        }
      
    }
}
