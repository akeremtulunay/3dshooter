using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BulletColorText : MonoBehaviour
{
    void Start()
    {
        GetComponent<Text>().text = "Default";
        if (FindObjectOfType<GameManager>().isShootingRed)
        {
            ChangeText();
        }
    }

    public void ChangeText()
    {
        if (GetComponent<Text>().text == "Default")
        {
            GetComponent<Text>().text = "Red";
        }
        else
        {
            GetComponent<Text>().text = "Default";
        }
    }
}
