using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpreadAngleText : MonoBehaviour
{
    void Start()
    {
        GetComponent<Text>().text = "" + 15;
    }

    public void SetText(float spreadAngle)
    {
        GetComponent<Text>().text = "" + spreadAngle;
    }
}
