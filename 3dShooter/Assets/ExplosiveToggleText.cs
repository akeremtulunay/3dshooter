using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExplosiveToggleText : MonoBehaviour
{

    void Start()
    {
        GetComponent<Text>().text = "Normal";
        if (FindObjectOfType<GameManager>().isShootingExplosive)
        {
            ChangeText();
        }
    }

    public void ChangeText()
    {
        if (GetComponent<Text>().text == "Normal")
        {
            GetComponent<Text>().text = "Explosive";
        }
        else
        {
            GetComponent<Text>().text = "Normal";
        }
    }   
}
