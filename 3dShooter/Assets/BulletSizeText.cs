using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BulletSizeText : MonoBehaviour
{
    void Start()
    {
        GetComponent<Text>().text = "Small";
        if (FindObjectOfType<GameManager>().isShootingBig)
        {
            ChangeText();
        }
    }

    public void ChangeText()
    {
        if (GetComponent<Text>().text == "Small")
        {
            GetComponent<Text>().text = "Big";
        }
        else
        {
            GetComponent<Text>().text = "Small";
        }
    }

}
